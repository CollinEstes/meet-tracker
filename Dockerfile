FROM node:12-alpine

RUN npm update npm -g

RUN mkdir -p /opt/app/node_modules

# copy package.json
WORKDIR /opt/app
COPY package*.json ./

# install dependencies
# --no-cache: download package index on-the-fly, no need to cleanup afterwards
# --virtual: bundle packages, remove whole bundle at once, when done
RUN apk --no-cache --virtual .build add \
  git \
  python \
  make \
  g++ \
  && npm ci \
  && apk del .build

# copy source files
COPY . .

ENV NODE_ENV=production
RUN npm run build

CMD ["npm", "run", "start"]
