const express = require('express');
const app = express();

const PORT = process.env.PORT || 3001;

const server = app.listen(PORT, function () {
    console.log(`server running on port ${PORT}`);
});

const io = require('socket.io')(server);

io.on('connection', function (socket) {
    // when new clients connect
    socket.on('REQUEST_FOR_UPDATE', function () {
        socket.broadcast.emit(`REQUEST_FOR_UPDATE`);
    })

    // when any admin sends an update
    socket.on('UPDATE', function (data) {
        socket.broadcast.emit('UPDATE', data);
    })
});